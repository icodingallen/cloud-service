pipeline {
    agent {
        node {
          label 'maven'
        }
    }

    //推送自动构建，可以为参数化赋值为当前版本等信息
    parameters {
        string(name:'TAG_NAME',defaultValue: 'v1.3',description:'项目标签版本')
        choice(
              description: '你需要选择哪个模块进行构建?',
              name: 'PROJECT',
              choices: ['user-service','movie-service']
        )
    }

    //
    //声明变量
    environment {
        DOCKER_CREDENTIAL_ID = 'aliyunhub'
        GITHUB_CREDENTIAL_ID = 'giteeid'
        KUBECONFIG_CREDENTIAL_ID = 'kubeconfig'
        REGISTRY = 'registry.cn-hangzhou.aliyuncs.com'
        DOCKERHUB_NAMESPACE = 'allenicodingdocker'
        GITHUB_ACCOUNT = 'icodingallen'
        //SONAR_CREDENTIAL_ID = 'sonar-token'
    }


    stages {
            stage ('代码检出') {
                steps {
                    checkout(scm)
                }
            }
            stage ('maven仓库存档') {
                steps {
                    container ('maven') {
                        sh 'mvn clean -gs `pwd`/configuration/settings.xml install'
                    }
                }
            }

            stage ('代码质量分析') {
                steps {
                    container ('maven') {
                        sh 'echo ok'
                    }
                }
            }
            stage ('构建镜像快照推送') {
                steps {
                    container ('maven') {
                        sh 'mvn -Dmaven.test.skip=true -gs `pwd`/configuration/settings.xml clean package'
                        sh 'docker build -f ./$PROJECT/Dockerfile -t $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER ./$PROJECT/target/'
                        withCredentials([usernamePassword(passwordVariable : 'DOCKER_PASSWORD' ,usernameVariable : 'DOCKER_USERNAME' ,credentialsId : "$DOCKER_CREDENTIAL_ID" ,)]) {
                            sh 'echo "$DOCKER_PASSWORD" | docker login $REGISTRY -u "$DOCKER_USERNAME" --password-stdin'
                            sh 'docker push $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER'
                        }
                    }
                }
            }
            stage('推送最新镜像'){
               when{
                 branch 'master'
               }
               steps{
                    container ('maven') {
                      sh 'docker tag  $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT:latest '
                      sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT:latest '
                    }
               }
            }

            stage('部署到dev环境（也可以是uat）') {
              when{
                branch 'master'
              }
              steps {
                input(id: 'deploy-to-dev', message: '部署到开发环境?')
                kubernetesDeploy(configs: "$PROJECT/deploy/dev/**", enableConfigSubstitution: true, kubeconfigId: "$KUBECONFIG_CREDENTIAL_ID")
              }
            }
            stage('推送生产版本'){
              when{
                expression{
                  return params.TAG_NAME =~ /v.*/
                }
              }
              steps {
                  container ('maven') {
                    input(id: 'release-image-with-tag', message: '推送生产版本镜像?')
                      withCredentials([usernamePassword(credentialsId: "$GITHUB_CREDENTIAL_ID", passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                        sh 'git config --global user.email "allen@icoding.com" '
                        sh 'git config --global user.name "allen" '
                        sh 'git tag -a $PROJECT-$TAG_NAME.$BUILD_NUMBER -m "$PROJECT - $TAG_NAME.$BUILD_NUMBER" '
                        sh 'git push http://$GIT_USERNAME:$GIT_PASSWORD@gitee.com/$GITHUB_ACCOUNT/cloud-service.git --tags --ipv4'
                      }
                    sh 'docker tag  $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT:$TAG_NAME '
                    sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/$PROJECT:$TAG_NAME '
                }
              }
            }
            stage('部署到生产环境') {
              when{
                expression{
                  return params.TAG_NAME =~ /v.*/
                }
              }
              steps {
                input(id: 'deploy-to-production', message: '部署到生产版本?')
                kubernetesDeploy(configs: "$PROJECT/deploy/prod/**", enableConfigSubstitution: true, kubeconfigId: "$KUBECONFIG_CREDENTIAL_ID")
              }
            }
    }
}