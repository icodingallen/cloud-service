package com.icoding.user.controller;

import com.icoding.user.feign.MovieFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    MovieFeignService movieFeignService;

    @GetMapping("/user/{id}/movie")
    public String getMyMovie(@PathVariable("id") String id){
        String hello = movieFeignService.hello();
        return "用户：【"+id+"】===> 最新电影："+hello;
    }
}
