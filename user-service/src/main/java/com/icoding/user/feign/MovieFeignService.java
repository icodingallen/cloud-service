package com.icoding.user.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 由于k8s的负载均衡网络，所以我们可以直接写Service地址
 */
@FeignClient(url = "http://movie-service.icoding-edu-backend",value = "movie-service")
public interface MovieFeignService {

    @GetMapping("/movie/latest")
    public String hello();
}
